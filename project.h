#ifndef PROJECT_H
#define PROJECT_H

#include <QObject>
#include <qmlapplicationviewer.h>

class Project : public QObject
{
    Q_OBJECT
public:
    explicit Project(QWidget *parent = 0);
    
signals:
    
public slots:
    void clean();
    void run();
    void checkErrors();
    void loadPlugins();

public:
    QmlApplicationViewer viewer;
};

#endif // PROJECT_H
