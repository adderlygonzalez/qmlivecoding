#ifndef COMPLETER_H
#define COMPLETER_H

#include <QListView>
#include <completerdelegate.h>

class Completer : public QListView
{
    Q_OBJECT
public:
    Completer(QWidget *parent = 0);

protected:
    void mouseReleaseEvent(QMouseEvent *e);

private:
    CompleterDelegate completerDelegate;

public slots:
    void activateCompleter();
    void hideCompleter();

signals:
    void selected(const QString& snippet);
};

#endif // COMPLETER_H
