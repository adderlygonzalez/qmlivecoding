#include "completer.h"
#include <QMouseEvent>

Completer::Completer(QWidget *parent) : QListView(parent)
{
    setFocusPolicy(Qt::NoFocus);
    setItemDelegate(&completerDelegate);
    setMaximumWidth(300);
}

void Completer::activateCompleter()
{
    show();
}

void Completer::hideCompleter()
{
    hide();
}

void Completer::mouseReleaseEvent(QMouseEvent *e)
{
    QModelIndex index = indexAt(e->pos());
    if (index.isValid())
        emit selected(index.data(Qt::UserRole).toString().arg(index.data().toString()));
}
