#include <QDir>
#include <QApplication>
#include <QDesktopWidget>
#include <QDeclarativeEngine>
#include <QDeclarativeContext>
#include "project.h"
#include "qmlapplicationviewer.h"

Project::Project(QWidget *parent) : QObject(parent), viewer(parent)
{
    viewer.setOrientation(QmlApplicationViewer::ScreenOrientationAuto);
    loadPlugins();
}

void Project::run()
{
    clean();
    viewer.setSource(QUrl::fromLocalFile("/data/data/org.PC.QMLiveCoding/main.qml"));
    checkErrors();
}


void Project::clean()
{
    viewer.engine()->clearComponentCache();
}

void Project::checkErrors()
{
    if (viewer.status() == QDeclarativeView::Error)
    {
        QStringList errorsList;
        foreach(const QDeclarativeError& error, viewer.errors())
            errorsList << error.toString();
        viewer.rootContext()->setContextProperty("errorsModel", QVariant::fromValue(errorsList));
        viewer.setMainQmlFile("qml/QMLiveCoding/errors.qml");
    }
}


void Project::loadPlugins()
{
    QDir pluginsFolder("assets:/plugins");
    QFileInfoList plugins = pluginsFolder.entryInfoList(QStringList() << "*.so");
    foreach(const QFileInfo& plugin, plugins)
    {
        QString targetName = QString("/data/data/org.PC.QMLiveCoding/%1").arg(plugin.fileName());
        if (!QFile::exists(targetName))
        {
            QFile source(plugin.filePath());

            QFile dest(targetName);
            if (dest.open(QIODevice::WriteOnly | QIODevice::Truncate) && source.open(QIODevice::ReadOnly))
            {
                dest.write(source.readAll());
            }
        }

        QString errorString;
        QString uri = plugin.baseName().replace("libqml", "").replace("plugin", "").prepend("Qt.labs.");
        viewer.engine()->importPlugin(targetName, uri, &errorString);
    }
}
