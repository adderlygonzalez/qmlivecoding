#ifndef COMPLETIONMODEL_H
#define COMPLETIONMODEL_H

#include <QColor>
#include <QAbstractItemModel>
#include <QStringList>

class CompletionModel : public QAbstractItemModel
{
public:
    CompletionModel();

    QModelIndex index(int row, int column, const QModelIndex &parent) const;
    QModelIndex parent(const QModelIndex &child) const;

    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;

    QVariant data(const QModelIndex &index, int role) const;

private:
    struct Suggestion
    {
        QString match;
        QString snippet;
        QColor color;

        bool operator < (const Suggestion& other) const
        {
            return match < other.match;
        }

        bool operator ==(const Suggestion& other) const
        {
            return match == other.match;
        }
    };

    QList<Suggestion> suggestions;

private:
    void addSuggestions(const char **words, const QString snippet, const QColor& color);
    void removeDuplicates(QList<Suggestion>& suggestionsList);

};

#endif // COMPLETIONMODEL_H
