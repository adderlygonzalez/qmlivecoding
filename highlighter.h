#ifndef HIGHLIGHTER_H
#define HIGHLIGHTER_H

#include <QSyntaxHighlighter>

class Highlighter : public QSyntaxHighlighter
{
public:
    Highlighter(QTextDocument *parent = 0);

protected:
    void highlightBlock(const QString &text);

private:
    void prepareHighlightingRules(const char** words, const QColor& color);
    void prepareColorRules(const char** words);
    void prepareLiteralRules(const QColor &color);
    void prepareCommentRules(const QColor &color);

private:
    struct HighlightingRule
    {
        QRegExp pattern;
        QTextCharFormat format;
    };
    QVector<HighlightingRule> highlightingRules;

};

#endif // HIGHLIGHTER_H
