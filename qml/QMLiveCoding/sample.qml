import QtQuick 1.1
import Qt.labs.shaders 1.0

Rectangle {
	id: root
	width: 360
	height: 360

	gradient: Gradient {
		GradientStop { position: 0.0; color: "blue" }
		GradientStop { position: 0.85; color: "yellow" }
		GradientStop { position: 1.0; color: "orange" }
	}

	ShaderEffectItem {
		anchors.fill: parent
		fragmentShader: "
			#ifdef GL_ES
			precision highp float;
			#endif

			uniform vec2 resolution;
			uniform float time;

			void main(void)
			{
			   float x = gl_FragCoord.x;
			   float y = gl_FragCoord.y;
			   float mov0 = x+y+cos(sin(time)*2.)*100.+sin(x/100.)*1000.;
			   float mov1 = y / resolution.y / 0.2 + time;
			   float mov2 = x / resolution.x / 0.2;
			   float c1 = abs(sin(mov1+time)/2.+mov2/2.-mov1-mov2+time);
			   float c2 = abs(sin(c1+sin(mov0/1000.+time)+sin(y/40.+time)+sin((x+y)/100.)*3.));
			   float c3 = abs(sin(c2+cos(mov1+mov2+c2)+cos(mov2)+sin(x/1000.)));
			   gl_FragColor = vec4( c1,c2,c3,1.0);
			}
		"

		property variant resolution: Qt.point(root.width, root.height)
		property real time : 0.0

		Timer {
			interval: 250; running: true; repeat: true
			onTriggered: parent.time += 1;
		}
	}

	Text {
		anchors.centerIn: parent
		text: "Hello World"
	}

	Text {
		anchors.right: parent.right;
		anchors.bottom: parent.bottom;
		anchors.margins: 5
		text: "version: 0.1.5"
	}
}
