#ifndef ITEMS_H
#define ITEMS_H

static const char* items[] = {
    "Item", "Component", "QtObject", "Rectangle", "Image", "BorderImage", "AnimatedImage", "Gradient", "GradientStop",
    "SystemPalette", "Text", "TextInput", "TextEdit", "IntValidator", "DoubleValidator", "RegExpValidator", "FontLoader",
    "MouseArea", "Keys", "FocusScope", "Flickable", "Flipable", "PinchArea", "Column", "Row", "Grid", "Flow", "Repeater",
    "Scale", "Rotation", "Translate", "State", "PropertyChanges", "StateGroup", "StateChangeScript", "ParentChange",
    "AnchorChanges", "Transition", "SequentialAnimation", "ParallelAnimation", "Behavior", "PropertyAction",
    "PauseAnimation", "SmoothedAnimation", "SpringAnimation", "ScriptAction", "PropertyAnimation", "NumberAnimation",
    "Vector3dAnimation", "ColorAnimation", "RotationAnimation", "ParentAnimation", "AnchorAnimation", "ListModel",
    "ListElement", "VisualItemModel", "VisualDataModel", "XmlListModel", "XmlRole", "Binding", "Package", "ListView",
    "GridView", "PathView", "Path", "PathLine", "PathQuad", "PathCubic", "PathAttribute", "PathPercent",
    "Connections", "Timer", "Qt", "WorkerScript", "Loader", "LayoutItem", "Particles", "ParticleMotionLinear",
    "ParticleMotionGravity", "ParticleMotionWander", "ShaderEffectItem", "ShaderEffectSource",
    NULL
};

#endif // ITEMS_H
