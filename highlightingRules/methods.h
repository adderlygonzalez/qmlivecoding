#ifndef METHODS_H
#define METHODS_H

const char* methods[] = {
    "childAt", "forceActiveFocus", "contains", "mapFromItem", "mapFromItem", "mapToItem", "mapToItem", "advance", "pause", "restart", "resume", "jumpTo", "angleBetween",
    "itemAt", "setSource", "cancelFlick", "flick", "resizeContent", "returnToBounds", "drop", "cancel", "start", "copy", "cut", "deselect", "insert", "positionAt", "isRightToLeft",
    "moveCursorSelection", "paste", "positionToRectangle", "redo", "remove", "select", "selectAll", "selectWord", "getText", "undo", "getFormattedText", "accept", "completeToBeginning",
    "completeToEnd", "reload", "modelIndex", "parentModelIndex", "addGroups", "create", "insert", "move", "get", "remove", "removeGroups", "resolve", "setGroups", "errorString", "reload",
    "itemAt", "decrementCurrentIndex", "incrementCurrentIndex", "indexAt", "positionViewAtBeginning", "positionViewAtEnd", "positionViewAtIndex", "itemAt", "indexAt", "moveCurrentIndexDown",
    "moveCurrentIndexLeft", "moveCurrentIndexRight", "moveCurrentIndexUp", "positionViewAtBeginning", "positionViewAtEnd", "positionViewAtIndex", "itemAt", "decrementCurrentIndex",
    "incrementCurrentIndex", "indexAt", "positionViewAtIndex", "openDatabaseSync", "scheduleUpdate", "restart", "start", "stop", "sendMessage", "getImageData", "save", "toDataURL",
    "cancelRequestAnimationFrame", "isImageError", "isImageLoaded", "isImageLoading", "loadImage", "requestAnimationFrame", "markDirty", "getContext", "requestPaint", "unloadImage",
    "createImageData", "drawImage", "arc", "arcTo", "beginPath", "bezierCurveTo", "clearRect", "clip", "closePath", "createConicalGradient", "createLinearGradient", "createRadialGradient",
    "ellipse", "fill", "fillRect", "fillText", "isPointInPath", "lineTo", "moveTo", "putImageData", "quadraticCurveTo", "rect", "reset", "resetTransform", "restore", "rotate", "roundedRect",
    "save", "scale", "setTransform", "shear", "stroke", "strokeText", "text", "transform", "translate", "createPattern", "measureText", "addColorStop",
    ,NULL
};

#endif // METHODS_H
