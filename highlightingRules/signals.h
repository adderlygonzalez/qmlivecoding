#ifndef SIGNALS_H
#define SIGNALS_H

static const char* _signals[] = {
    "onLineLaidOut", "onLinkActivated", "onItemAdded", "onItemRemoved", "onLoaded", "onCanceled", "onClicked", "onDoubleClicked", "onEntered", "onExited", "onPositionChanged",
    "onPressAndHold", "onPressed", "onReleased", "onWheel", "onAsteriskPressed", "onBackPressed", "onBacktabPressed", "onCallPressed", "onCancelPressed", "onContext1Pressed",
    "onContext2Pressed", "onContext3Pressed", "onContext4Pressed", "onDeletePressed", "onDigit0Pressed", "onDigit1Pressed", "onDigit2Pressed", "onDigit3Pressed", "onDigit4Pressed",
    "onDigit5Pressed", "onDigit6Pressed", "onDigit7Pressed", "onDigit8Pressed", "onDigit9Pressed", "onDownPressed", "onEnterPressed", "onEscapePressed", "onFlipPressed",
    "onHangupPressed", "onLeftPressed", "onMenuPressed", "onNoPressed", "onPressed", "onReleased", "onReturnPressed", "onRightPressed", "onSelectPressed", "onSpacePressed",
    "onTabPressed", "onUpPressed", "onVolumeDownPressed", "onVolumeUpPressed", "onYesPressed", "onFlickEnded", "onFlickStarted", "onMovementEnded", "onMovementStarted",
    "onPinchFinished", "onPinchStarted", "onPinchUpdated", "onCanceled", "onGestureStarted", "onPressed", "onReleased", "onTouchUpdated", "onUpdated", "onDropped", "onEntered",
    "onExited", "onPositionChanged", "onAccepted", "onLinkActivated", "onChanged", "onAdd", "onRemove", "onDragEnded", "onDragStarted", "onFlickEnded", "onFlickStarted",
    "onMovementEnded", "onMovementStarted", "onTriggered", "onMessage", "onImageLoaded", "onPaints", "onPainted",
    NULL
};


#endif // SIGNALS_H
