#ifndef MODULES_H
#define MODULES_H

static const char* modules[] = {
    "QtQuick 1.0", "QtQuick 1.1", "QtQuick 2.0", "QtQuick.Window 2.0",
    "Qt.labs.shaders 1.0", "Qt.labs.gestures 1.0", "Qt.labs.particles 1.0", "Qt.labs.folderlistmodel 1.0",
    NULL };

#endif // MODULES_H
