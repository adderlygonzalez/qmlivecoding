#include "completionmodel.h"
#include "highlightingRules/types.h"
#include "highlightingRules/items.h"
#include "highlightingRules/bindings.h"
#include "highlightingRules/keywords.h"
#include "highlightingRules/modules.h"
#include "highlightingRules/signals.h"
#include "highlightingRules/colors.h"

CompletionModel::CompletionModel()
{
    addSuggestions(types, "%1$", QColor::fromRgb(255, 255, 85));
    addSuggestions(items, "%1 {\n$\n}", Qt::green);
    addSuggestions(bindings, "%1$", QColor::fromRgb(255, 85, 85));
    addSuggestions(keywords, "%1$", QColor::fromRgb(255, 255, 85));
    addSuggestions(modules, "%1$", Qt::blue);
    addSuggestions(_signals, "%1$", QColor::fromRgb(255, 255, 85));
    addSuggestions(colors, "\"%1\"$", QColor::fromRgb(255, 85, 255));
    qSort(suggestions.begin(), suggestions.end());
    removeDuplicates(suggestions);
}

QModelIndex CompletionModel::index(int row, int column, const QModelIndex &parent) const
{
    if (parent.isValid())
        return QModelIndex();
    else
        return createIndex(row, column);
}

QModelIndex CompletionModel::parent(const QModelIndex &child) const
{
    Q_UNUSED(child);
    return QModelIndex();
}

int CompletionModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    else
        return suggestions.count();
}

int CompletionModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    else
        return 1;
}

QVariant CompletionModel::data(const QModelIndex &index, int role) const
{
    if (index.isValid() && index.row() >= 0 && index.row() < suggestions.count())
    {
        Suggestion suggestion = suggestions[index.row()];
        if (role == Qt::DisplayRole)
            return QVariant(suggestion.match);
        if (role == Qt::TextColorRole)
            return QVariant::fromValue<QColor>(suggestion.color);
        if (role == Qt::UserRole)
            return QVariant(suggestion.snippet);
    }

    return QVariant();
}

void CompletionModel::addSuggestions(const char **words, const QString snippet, const QColor &color)
{
    Suggestion suggestion;
    const char** current = words;
    while (*current)
    {
        suggestion.match = *current;
        suggestion.snippet = snippet;
        suggestion.color = color;
        suggestions << suggestion;
        current++;
    }
}

void CompletionModel::removeDuplicates(QList<CompletionModel::Suggestion> &suggestionsList)
{
    for (int i = suggestions.count() - 1; i >= 1; i--)
    {
        if (suggestionsList[i] == suggestionsList[i - 1])
            suggestionsList.removeAt(i);
    }
}
